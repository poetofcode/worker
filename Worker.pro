#-------------------------------------------------
#
# Project created by QtCreator 2013-02-06T11:09:34
#
#-------------------------------------------------

QT       += core gui

TARGET = Worker
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp

HEADERS  += dialog.h

FORMS    += dialog.ui
