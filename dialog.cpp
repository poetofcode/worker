#include "dialog.h"
#include "ui_dialog.h"

#include <QTimer>
#include <QDebug>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_clicked()
{
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateTime()));
    timer->start(1000);
}

void Dialog::updateTime()
{
    QTime time = ui->timeEdit->time();
    ui->timeEdit->setTime(time.addSecs(-1));
}
